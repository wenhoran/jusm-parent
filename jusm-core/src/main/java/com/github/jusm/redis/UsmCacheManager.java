package com.github.jusm.redis;

public final class UsmCacheManager {
	
	public static final String CACHE_NAME_PREFIX = "usm:";
	
	
	public static final String PARAMTER = CACHE_NAME_PREFIX + "parameter";
	
	
	public static final String PERMISSION = CACHE_NAME_PREFIX + "permission";	
	
	
	public static final String USER = CACHE_NAME_PREFIX + "user";	
	 
}
